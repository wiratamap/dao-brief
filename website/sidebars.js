/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

module.exports = {
  homeSidebar: [
    'home',
    'home/about',
    'home/get-in-touch',
  ],

  toolsSidebar: [
    'tools-and-frameworks',
    'tools-and-frameworks/framework1',
    'tools-and-frameworks/tool1',
    {
      type: 'category',
      label: 'Tool Type1',
      collapsed: true,
      items: [ 
        'tools-and-frameworks/tool-type1/tool-type1-test1'
      ],
    },
  ],

  bestPracticesSidebar: [
    'best-practices',
    'best-practices/test-practice1',
    {
      type: 'category',
      label: 'Practice Type1',
      collapsed: true,
      items: [ 
        'best-practices/practice-type1/practice-type1-test1'
      ],
    },
  ],

  daosSidebar: [
    'daos',
    'daos/daos-test1',
    'daos/daos-test2',
    {
      type: 'category',
      label: 'Protocol DAOs',
      collapsed: true,
      items: [
        'daos/protocol-daos/protocol-dao-test1'
      ],
    },
  ],

  resourcesSidebar: [
    'resources',
    'resources/test-resource1',
    {
      type: 'category',
      label: 'Resource Type1',
      collapsed: true,
      items: [
        'resources/resource-type1/resource-type1-test1'
      ],
    },
  ],
}
