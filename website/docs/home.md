---
id: home
title: The DAO Brief
slug: /
---

Hello Agent. Your mission begins here.

<img  src={require('../static/img/DIA_avatar.png').default}  alt="DIA Logo"/>
