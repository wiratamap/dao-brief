const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Decentral Intelligence Agency',
  tagline: 'DAO Brief',
  url: 'https://decentral-intelligence-agency.gitlab.io',
  baseUrl: '/dao-brief/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'decentral-intelligence-agency', // Usually your repo name.
  themeConfig: {
    navbar: {
      hideOnScroll: true,
      title: 'Decentral Intelligence Agency',
      logo: {
        alt: 'DIA',
        src: 'img/logo.svg',
      },
      items: [
	{
          type: 'doc',
          docId: 'tools-and-frameworks',
          position: 'left',
          label: 'Tools & Frameworks',
        },
	{
          type: 'doc',
          docId: 'best-practices',
          position: 'left',
          label: 'Best Practices',
        },
        {
          type: 'doc',
          docId: 'daos',
          position: 'left',
          label: 'List of DAOs',
        },

        {
          type: 'doc',
          docId: 'resources',
          position: 'left',
          label: 'Resources',
        },
        {
          href: 'https://gitlab.com/decentral-intelligence-agency/dao-briefs',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Tools & Frameworks',
              to: '/tools-and-frameworks',
            },
            {
              label: 'Best Practices',
              to: '/best-practices',
            },
            {
              label: 'List of DAOs',
              to: '/daos',
            },
            {
              label: 'Resources',
              to: '/resources',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/9HwQerG8H2',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/decentral-intelligence-agency/dao-briefs',
            },
          ],
        },
      ],
      copyright: `Built with Docusaurus.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/decentral-intelligence-agency/dao-briefs',
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
